/*
 *File: snap.c
 *Creator: Muskaan Mendiratta
 *Created: Wed Nov 21 2018
 */

#include "general.h"

//initialize SNAP_Tuple?

extern SNAP_Tuple* SNAP_lookup(SNAP_Tuple* relation, SNAP_Tuple tuple){
  int index = hashInt(tuple->StudentId, TABLE_SIZE);
  SNAP_Tuple this = relation[index];
  if (this->Name == NULL){
    return NULL; //entry does not exist
  }
  while (this->next != NULL){
    if (this->StudentId == tuple->StudentId &&
	strcmp (this->Name, row->Name) == 0 &&
	strcmp (this->Address, tuple->Address) == 0 &&
	strcmp(this->Phone, tuple->Phone) == 0){
      printf ("(%d, %s, %s, %s) found at index: %d\n", this->StudentId, this->Name, this->Address, this->Phone, index);
      return this;
    }
    this = this->next;
  }
  printf("%s\n", "Entry could not be found");
  return NULL; //could not be found
}

extern void SNAP_insert(SNAP_Tuple* relation, SNAP_Tuple tuple){
  int index = hashInt(tuple->StudentId, TABLE_SIZE);
  if (SNAP_lookup(SNAP_Tuple*relation, SNAP_Tuple tuple) != NULL){
    printf("%s\n", "Entry already exists at index: %d\n", index);
    return;
  }
  SNAP_Tuple this = relation[index];
   memcpy(this, tuple, sizeof(SNAP_Tuple));
}

extern void SNAP_delete(SNAP_Tuple* relation, SNAP_Tuple tuple){
  int index = hashInt(tuple->StudentId, TABLE_SIZE);
  if (SNAP_lookup(relation, tuple) == NULL){
    printf("%s\n", "Entry does not exist");
    return;
  }
  tuple->StudentId = 0;
  tuple->Name = NULL;
  tuple->Address = NULL;
  tuple->Phone = NULL;
  printf("%s\n", "Record deleted");
}

extern void writeSNAPRelation(SNAP_tuple* relation[]){
  FILE *SNAP_File;
  SNAP_File = fopen("SNAP.txt", "w");
  
  if (SNAP_File == NULL){
    printf("%s\n", "File does not exist");
  }
  SNAP_Tuple tuple;
  for (int i = 0; i < TABLE_SIZE; i++){
    tuple = relation[i];
    if (tuple->StudentId != 0){
      fprintf(SNAP_File, "%d\t%s\t%s\t%s\n", tuple->StudentId, tuple->Name, tuple->Address, tuple->Phone);
      while(tuple->next != NULL){
	fprintf(SNAP_File, "%d\t%s\t%s\t%s\n", tuple->StudentId, tuple->Name, tuple->Address, tuple->Phone);
      }
    }
  }
  printf("%s\n", "Relation written to file");
  fclose(SNAP_File);
}

extern void SNAP_read(SNAP_tuple* relation[], char* fileName){
  FILE *SNAP_File;

  int StudentId = 0;
  char Name[255];
  char Address[255];
  char Phone[255];
  SNAP_File = fopen(fileName, "r");
  if (SNAP_File == NULL){
    printf("%s\n", "File not found");
    return;
  }
  SNAP_tuple tuple;

  while (!feof (SNAP_File)){
    fscanf(SNAP_File, "%d\t%s\t%s\t%s", i, Name, StudentId, Address, Phone);
    tuple->Name = Name;
    tuple->StudentId = StudentId;
    tuple->Address = Address;
    tuple->Phone = Phone;
    SNAP_insert(relation, tuple);
  }
  fclose(SNAP_File);
  return;
}

extern void SNAP_print(SNAP_Tuple* relation[]){
  SNAP_tuple tuple;

  for(int i = 0; i < TABLE_SIZE; i++){
    tuple = relation[i];
    if (tuple->Name != NULL){
      printf("%d\t%s\t%s\t%s\n", tuple->StudentId, tuple->Name, tuple->Address, tuple->Phone);
      while(tuple->next != NULL){
	tuple = tuple->next;
	printf("%d\t%s\t%s\t%s\n", tuple->StudentId, tuple->Name, tuple->Address, tuple->Phone);
      }
    }
  }
  printf("\n");
}
	
