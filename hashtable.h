/*
 *File: hashtable.h
 *Creator: Muskaan Mendiratta
 *Created: Mon Oct 19 2018
 */

#ifndef _hashtable_h
#define _hashtable_h_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct HT_item *HT_item;
typedef struct HT *HT;

/*
 *Insert an entry into the hashtable given the element and the sie
 */
extern void insert(Hashtable H, int size);



#endif
