/*
 *File: part2.c
 *Creator: Muskaan Mendiratta
 *Created: Fri Nov 23 2018
 */
#include "general.h"
/*
 *Get StudentId when the name of the student is given
 */
extern SNAP_Tuple* SNAP_getTuple(SNAP_Tuple* relation[], char* name){
  SNAP_Tuple tuple;
  for (int i = 0; i < TABLE_SIZE; i++){
    tuple = relation[i];
    while(tuple->next != NULL){
      if (strcmp(tuple->Name, name)){
	return tuple;
      }else{
	tuple = tuple->next;
      }
    }
  }
  return NULL;
}

extern CSG_Tuple* CSG_getTuple(
extern char* SNAP_CSG_getGrade(char* name, char* course, SNAP_Tuple* relation1[], CSG_Tuple relation2[]){
  SNAP_Tuple tuple = SNAP_getTuple(relation, name);
  if (tuple == NULL){
    printf("%s\n", "Entry does not exist");
  }
  int index = hashIntAndString(course, 6, id, TABLE_SIZE);
  CSG_tuple tuple = relation[index];
  while (tuple->next != NULL){
    if (strcmp(tuple->Course, course) == 0 && tuple->StudentId == id){
      return tuple;
    }
    tuple = tuple->next;
  }
  return NULL;
}

extern CSG_Tuple* CSG_getTuple2(CSG_Tuple* relation[], int id){
    CSG_Tuple* tuple1 = (CSG_Tuple*)malloc(sizeof(CSG_Tuple));
    CSG_Tuple* tuple2 = (CSG_Tuple*)malloc(sizeof(CSG_Tuple));

    for (int i = 0; i < TABLE_SIZE; i++){
      CSG_tuple tuple = relation[i];
      if (tuple->StudentId == id){
	return tuple;
      }
    }
    return NULL;
}
extern CDH_Tuple* CDH_getTuple(CDH_Tuple* relation[], char*course, char* day){
  int index = hashtwoStrings(course, 6, day, 6, TABLE_SIZE);
  CDH_Tuple tuple = relation[index];
  if (tuple->Course == NULL){
    System.out.println("%s\n", "Course not found");
    return NULL;
  }
  while (tuple->next != NULL){
    if (strcmp(tuple->Course, course) == 0){
      return tuple;
    }
    tuple = tuple->next;
  }
  return NULL;
}

extern CR_Tuple* CR_getTuple(CR_Tuple* relation[], char* course){
  int index = hashString(course, 6, TABLE_SIZE);
  CR_Tuple* tuple;
  if (tuple->course == NULL){
    return NULL;
  }
  while(tuple->next != NULL){
    if (strcmp(tuple->Course, course) == 0){
      return tuple;
    }
    tuple = tuple->next;
  }
}

extern char* part2a(char* name, char* course, SNAP_Tuple* relation[], CSG_Tuple* relation2[]){
  SNAP_Tuple tuple = SNAP_getTuple(name, relation);
  if (tuple == NULL){
    printf("%s\n", "Entry does not exist in SNAP");
    return NULL;
  }
  int id = projectId(tuple);
  CSG_Tuple tuple2 = CSG_getTuple(id, course, relation2);
  char* grade = projectGrade(tuple2);
  if (grade == NULL){
    printf("%s\n", "Entry does not exist in CSG");
  }
  return grade;
}


      
    

