/*
 *File: general.h
 *Creator: Muskaan Mendiratta
 *Created: Wed Oct 21 2018
 */

/*
 *Relations: SNAP, CSG, CP, CDH
 */

extern int TABLE_SIZE = 1009;

typedef struct SNAP_Tuple{
  int StudentId;
  char Name[30];
  char Address[50];
  char Phone[8];
  SNAP_Tuple next;
}SNAP_Tuple;


typedef struct CSG_Tuple{
  int StudentId;
  char Course[6];
  char Grade[3];
  CSG_Tuple next;
}CSG_Tuple;


typedef struct CP_Tuple{
  char Course[6];
  char Prereq[6];
  CP_Tuple next;
}CP_Tuple;


typedef struct CDH_Tuple{
  char Course[6];
  char Day[2];
  char Hour[4];
  CDH_Tuple next;
}CDH_Tuple;

extern int hashIntString(char str[], int strSize, int id, int hasher);

extern int hashInt(int id, int hasher);

extern int hashTwoStrings(char str1[], char str2[], int str1Size, int str2Size, int hasher);

extern int hashString (char str[], int strSize, int hasher);

/*
 *Functions for SNAP
 *print, insert, delete, lookup
 */
extern void SNAP_print(SNAP_Tuple* relation);
extern void SNAP_insert(SNAP_Tuple* relation, SNAP_Tuple tuple);
extern void SNAP_delete(SNAP_Tuple* relation, SNAP_Tuple tuple);
extern SNAP_Tuple* SNAP_lookup(SNAP_Tuple* relation, SNAP_Tuple tuple);
  

/*
 *Functions for CSG
 *print, insert, delete, lookup
 */
extern void CSG_print(CSG_Tuple* relation);
extern void CSG_insert(CSG_Tuple* relation, CSG_Tuple tuple);
extern void CSG_delete(CSG_Tuple* relation, CSG_Tuple tuple);
extern CSG_Tuple* CSG_lookup(CSG_Tuple* relation, CSG_Tuple tuple);
  

/*
 *Functions for CP
 *print, insert, delete, lookup
 */
extern void CP_print(CP_Tuple* relation);
extern void CP_insert(CP_Tuple* relation, CP_Tuple tuple);
extern void CP_delete(CP_Tuple* relation, CP_Tuple tuple);
extern CP_Tuple* CP_lookup(CP_Tuple* relation, CP_Tuple tuple);
  

/*
 *Functions for CDH
 *print, insert, delete, lookup
 */
extern void CDH_print(CDH_Tuple* relation);
extern void CDH_insert(CDH_Tuple* relation, CDH_Tuple tuple);
extern void CDH_delete(CDH_Tuple* relation, CDH_Tuple tuple);
extern CDH_Tuple* CDH_lookup(CDH_Tuple* relation, CDH_Tuple tuple);

//TODO
/* select, project join */
  
  
  
