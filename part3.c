/*
 *File: part3.c
 *Creator: Muskaan Mendiratta
 *Created: Sat November 24 2018
 */

#import "general.h"

extern CRDH_Tuple* join (CR_Tuple* relation[], CDH_Tuple* relation2[]){
  CRDH_Tuple CRDH_tuple = (CRDH_Tuple*)malloc(TABLE_SIZE * sizeof(CRDH_Tuple));
  for (int i = 0; i < TABLE_SIZE; i++){
      
    CRDH_Tuple tuple = (CRDH_Tuple*)malloc(sizeof(CRDH_Tuple));
    tuple->Course = (char*)malloc(7 * sizeof(char));
    tuple->Day = (char*)malloc(2 * sizeof(char));
    tuple->Hour = (char*)malloc(5 * sizeof(char));

    CDRH_Tuple* tuple_pointer = relation2[i];

    if (tuple_pointer->course == NULL){
      CRDH_tuple[i] = *tuple;
      continue;
    }
    if (strcmp(tuple_pointer->Course, "") != 0){
      char* courseName = tuple_pointer->Course;
      CR_Tuple* CR_tuple = NULL;
      for (int j = 0; j < TABLE_SIZE; j++){
	if (relation[j] != NULL){
	  if (relation[j]->Course != NULL){
	    if (strcmp(relation[j]->Course, courseName) == 0){
		CR_tuple = relation[j];
	    }
	  }
	}
      }

      while (CR_tuple != NULL){
	tuple->Course = tuple_pointer->Course;
	tuple->Day = tuple_pointer->Day;
	tuple->Hour = tuple_pointer->Hour;
	tuple->Room = CR_tuple->Room;
	CR_tuple = CR_tuple->next;
      }
    }
    relation[i] = *tuple;
  }
  return CRDH_tuple;
}


CSG_Tuple* project (CSG_Tuple* relation[], char* attr){
  bool showCourse = false;
  bool showStudent = false;
  bool showGrade = false;
  int i = 0;
  char* attribute = attr[i];
  while(strcmp(attribute, "") != 0){
    if (strcmp(attribute, "Course") == 0){
      showCourse = true;
    }
    if (strcmp(attribute, "StudentId") == 0){
      showStudent = true;
    }
    if(strcmp(attribute, "Grade") == 0){
      showGrade = true;
    }
    i++;
    attribute = attr[i];
  }
  CSG_Tuple* tuple_pointer;
  CSG_Tuple* tuple = malloc(TABLE_SIZE * sizeof(CSG_Tuple));
  for (int i = 0; i < TABLE_SIZE; i++){
    tuple[i]->next = NULL;
  }
  CSG_Tuple* pointer;

  for (int i = 0; i < TABLE_SIZE; i++){
    if (showCourse){
      strcpy(tuple[i]->Course, relation[i]->Course);
    }else{
      strcpy(tuple[i]->Course, "");
    }

    if (showStudent){
      tuple[i]->StudentId = relation[i]->StudentId;
    }else{
      tuple[i]->StudentId = 0;
    }

    if (showGrade){
      strcpy(tuple[i]->Grade, relation[i]->Grade);
    }else{
      strcpy(tuple[i]->Grade, "");
    }
  }
  return tuple;
}

	     
	     
  
