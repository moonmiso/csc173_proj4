
/*
 *File: general.c
 *Creator: Muskaan Mendiratta
 *Created: Thur November 22 2018
 */
int stringToint (char* str, int strSize){
  int strInt;
  int count = sizeof(char);
  for (int i = 0; i < strSize; i++){
    int adder= (int) str[0];
    strInt += adder;
    str += count;
  }
  return strInt;
}
extern int hashIntString (char str[], int strSize, int id, int hasher){
  int hashedString = stringToInt(str, strSize);
  hashedString = (id+stringToInt) % hasher;
  return hashedString;
}

extern int hashInt(int id, int hasher){
  int hash = id%hasher;
  return hash;
}

extern int hashTwoStrings(char str1[], char str2[], int str1Size, int str2Size, int hasher){
  char* hashed1 = str1;
  int hash1 = stringToInt(hashed1, str1Size);

  char* hashed2 = str2;
  int hash2 = stringToInt(hashed2, str2Size);

  int hash = (hash1+hash2)%hasher;
  return hash;
}
