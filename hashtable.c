/*
 *File: hashtable.c
 *Creator: Muskaan Mendiratta
 *Created: Mon Oct 19 2018
 */

#include "hashtable.h"

/*
 *Hashtable buckets struct
 */
struct HT_item{
  char* key;
  char* value;
};

struct HT{
  int size;
  int count;
  HT_item* items;
}
extern HT new_HT(){
  HT this = (HT)malloc(sizeof(struct HashTable));
  if (this == NULL){
    return NULL;   //Out of memory
  }
  
